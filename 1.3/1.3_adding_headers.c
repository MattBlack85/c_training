#include <stdio.h>

main()
{
  	float celsius, fahr;
	int start, end, step;

	start = 0;
	end = 100;
	step = 10;

	celsius = start;
	printf("°C\t°F\n");

	while (fahr <= end) {
		celsius = (5.0/9.0) * (fahr - 32.0);
		printf("%3.0f %6.1f\n", fahr, celsius);
		fahr = fahr + step;
	}
}
