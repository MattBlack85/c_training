#include <stdio.h>

main()
{
	float celsius, fahr;
	int start, end, step;

	start = 0;
	end = 100;
	step = 10;

	celsius = start;
	printf("°C\t°F\n");

	while (celsius <= end) {
		fahr = ((9.0/5.0)*celsius) + 32.0;
		printf("%3.0f %6.1f\n", celsius, fahr);
		celsius = celsius + step;
	}
}
